#!/usr/bin/env python3

import RPi.GPIO as GPIO
import time
from rpi_ws281x import *
import argparse
import time
import random
import pygame

millis = lambda: int(round(time.time() * 1000))

LED_COUNT      = 3      # Number of LED pixels.
LED_PIN_1      = 12
LED_PIN_2      = 13     
LED_PIN_3      = 19     # X
LED_PIN_4      = 18     # X
LED_FREQ_HZ    = 800000 # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10     # DMA channel to use for generating a signal (try 10)
LED_BRIGHTNESS = 150    # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False  # True to invert the signal (when using NPN transistor level shift)

BUTTON_PIN_1 = 1
BUTTON_PIN_2 = 7
BUTTON_PIN_3 = 8        # X
BUTTON_PIN_4 = 25       # X

LED_MAPPING = [LED_PIN_1, LED_PIN_2]
BUTTON_MAPPING = [BUTTON_PIN_1, BUTTON_PIN_2]

BUTTON_PIN_MUTE = 11
BUTTON_PIN_MAIN = 9

GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN_1, GPIO.OUT)
GPIO.setup(LED_PIN_2, GPIO.OUT)
GPIO.setup(BUTTON_PIN_1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(BUTTON_PIN_2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(BUTTON_PIN_MUTE, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(BUTTON_PIN_MAIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

previous_millis = 0
current_millis = 0

def colorOn(strip, color):
    """Turn led strip to that color"""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(color.r, color.b, color.g))
    strip.show()
    return 0


def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    previous_millis = millis()
    current_millis = millis()
    cur_loops = 0;

    while True:
        current_millis = millis()
        if current_millis - previous_millis >= wait_ms:
            previous_millis = current_millis;

            strip.setPixelColor(cur_loops, Color(color.r, color.b, color.g))
            strip.show()

            cur_loops = cur_loops + 1
            if cur_loops == strip.numPixels():
                break
    return 0

def colorFade(strip, color, steps, wait_ms=50, correct_button_check=0):
    """Flash a color, then slowly fade"""

    previous_millis = millis()
    current_millis = millis()
    cur_loops = 0;

    while True:
        current_millis = millis()
        if current_millis - previous_millis >= wait_ms:
            previous_millis = current_millis;

            for i in range(strip.numPixels()):
                strip.setPixelColor(i, Color(color.r-int(color.r/(steps-cur_loops)), color.b-int(color.b/(steps-cur_loops)), color.g-int(color.g/(steps-cur_loops))))
                strip.show()

            cur_loops = cur_loops + 1
            if cur_loops == steps:
                break

        if correct_button_check > 0:
            if GPIO.input(BUTTON_PIN_1) == GPIO.HIGH:
                colorOn(strip, Color(0, 0, 0))
                if BUTTON_MAPPING[correct_button_check-1] == BUTTON_PIN_1:
                    return -1
                else:
                    return 1

            if GPIO.input(BUTTON_PIN_2) == GPIO.HIGH:
                colorOn(strip, Color(0, 0, 0))
                if BUTTON_MAPPING[correct_button_check-1] == BUTTON_PIN_2:
                    return -1
                else:
                    return 2

    return 0

def colorFlashing(strip, color, times, wait_ms=50):
    """Flash a color, then slowly fade"""
    flash_on = 1

    previous_millis = millis()
    current_millis = millis()
    cur_loops = 0;

    while True:
        current_millis = millis()
        if current_millis - previous_millis >= wait_ms:
            previous_millis = current_millis;

            if flash_on == 1:
                for i in range(strip.numPixels()):
                    strip.setPixelColor(i, Color(color.r, color.b, color.g))
            else:
                for i in range(strip.numPixels()):
                    strip.setPixelColor(i, Color(0, 0, 0))
            strip.show()

            if flash_on == 1:
                flash_on = 0
            else:
                flash_on = 1
                cur_loops = cur_loops + 1
                if cur_loops == times:
                    break 

    return 0

# ~~

wack_ms = 50

# The true wackamole experience

def wackamole(strip_mapping):

    previous_millis = millis()
    current_millis = millis()

    score = 0
    fouls = 0

    while fouls < 3:

        wait_steps = 0
        if score == 0 and fouls == 0:
            wait_steps = 50
        elif score > 10:
            wait_steps = random.randint(5, 15)
        elif score > 5:
            wait_steps = random.randint(10, 30)
        else:
            wait_steps = random.randint(15, 45)

        waiting_done = 0
        too_soon = 0

        while True:
            current_millis = millis()
            if current_millis - previous_millis >= wack_ms:
                previous_millis = current_millis;

                wait_steps = wait_steps-1
                if wait_steps == 0:
                    waiting_done = 1

            if GPIO.input(BUTTON_PIN_1) == GPIO.HIGH:
                    waiting_done = 1
                    too_soon = 1
            if GPIO.input(BUTTON_PIN_2) == GPIO.HIGH:
                    waiting_done = 1
                    too_soon = 2

            if waiting_done == 1:
                if too_soon > 0:
                    colorFlashing(strip_mapping[too_soon-1], Color(255, 0, 0), 3, 150)
                    fouls = fouls + 1
                else:
                    rand = random.randint(1, 2)
                    result = colorFade(strip_mapping[rand-1], Color(0, 255, 255), 10, 100, rand)
                    if result == -1:
                        colorFlashing(strip_mapping[rand-1], Color(0, 0, 255), 3, 150)
                        score = score + 1
                    elif result == 0:
                        colorFlashing(strip_mapping[rand-1], Color(255, 0, 0), 3, 150)
                        fouls = fouls + 1
                    else:
                        colorFlashing(strip_mapping[result-1], Color(255, 0, 0), 3, 150)
                        fouls = fouls + 1
                break

            if GPIO.input(BUTTON_PIN_MUTE) == GPIO.HIGH:
                colorOn(strip_mapping[0], Color(0, 0, 0))
                colorOn(strip_mapping[1], Color(0, 0, 0))
                fouls = 3
                break

    print('SCORE: ' + str(score))

# Music related stuff

def play_music():
    if not pygame.mixer.music.get_busy():
        pygame.mixer.music.load('revita.mp3')  # Replace 'drum.mp3' with the path to your file
        pygame.mixer.music.set_volume(0.75)  # Adjust this value between 0.0 and 1.0 to control the volume
        pygame.mixer.music.play()
        print("enjoy music :)")

def stop_music():
    if pygame.mixer.music.get_busy():
        pygame.mixer.music.set_volume(0.00)
        pygame.mixer.music.stop()
        print("enjoy no music :(")

# Main program logic follows

if __name__ == '__main__':
    pygame.mixer.init()

    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    strip_1 = Adafruit_NeoPixel(LED_COUNT, LED_PIN_1, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, 0)
    strip_1.begin()
    strip_2 = Adafruit_NeoPixel(LED_COUNT, LED_PIN_2, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, 1)
    strip_2.begin()

    strip_mapping = [strip_1, strip_2]

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    previous_millis = millis()
    current_millis = millis()

    try:
        while True:
            if GPIO.input(BUTTON_PIN_1) == GPIO.HIGH: # wackamole button?
                colorFlashing(strip_mapping[0], Color(0, 0, 255), 2, 150)
                time.sleep(1)
                print("game start!")
                wackamole(strip_mapping)
            if GPIO.input(BUTTON_PIN_MAIN) == GPIO.HIGH: # music button?
                play_music()
            if GPIO.input(BUTTON_PIN_MUTE) == GPIO.HIGH: # mute button?
                stop_music()

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip_1, Color(0, 0, 0), 10)
            colorWipe(strip_2, Color(0, 0, 0), 10)
