The code used in Solemates, and specifically for playing music on entrance & wack-a-mat through the raspberry pi
The two files running python are the following:
- wacka.py: the intended program to run, playingn music when the main button is pressed & playing a simplified wack-a-mole game.
- faking.py: a "fake" program where the user manually inputs commands to make the buttons light up in different patterns.
main python libraries used are mygame (for playing music) & RPi (for controlling input/output from the raspberry pi)